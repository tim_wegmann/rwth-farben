#!/usr/bin/python
# Test des Farbkatalogs
# (c) 2017 Lukas Koschmieder <lukas.koschmieder@rwth-aachen.de>

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math
import yaml

with open('rwth-farben.yml', 'r') as stream:
    colors = yaml.safe_load(stream)

fig = plt.figure("RWTH Farben")
plt.title('Visualisierung der Hex- (links) und RGB-Werte (rechts)')
plt.axis('off')

ax = fig.add_subplot(111)

x_count = float(5) # Shades per color
y_count = float(len(colors)) / x_count
x = 0
y = 0
w = 1 / x_count
h = 1 / y_count

for key, value in sorted(colors.iteritems()):
    pos = (x / x_count, y / y_count)
    ax.add_patch(patches.Rectangle(pos, w, h, facecolor=(float(value['r']) / 255, float(value['g']) / 255, float(value['b']) / 255), linewidth=2, edgecolor='w'))
    ax.add_patch(patches.Rectangle(pos, w / 2, h, facecolor=value['hex'], linewidth=2, edgecolor='w'))
    ax.annotate(key, xy=pos, size=8, bbox=dict(facecolor='w'))
    if x >= x_count - 1:
        y += 1
        x = 0
    else:
        x += 1

plt.show()
